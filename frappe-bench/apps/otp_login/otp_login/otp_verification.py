import frappe
from frappe import _
import pyotp


class ExpiredLoginException(Exception): pass


def confirm_otp_token(login_manager):
	'''Confirm otp matches.'''
	otp = frappe.form_dict.get('otp')
	tmp_id = frappe.form_dict.get('tmp_id')
	hotp_token = frappe.cache().get(tmp_id + '_token')
	otp_secret = frappe.cache().get(tmp_id + '_otp_secret')
	if not otp_secret:
		raise ExpiredLoginException(_('Login session expired, login again'))
	hotp = pyotp.HOTP(otp_secret)
	if hotp_token:
		if hotp.verify(otp, int(hotp_token)):
			frappe.cache().delete(tmp_id + '_token')
			return True
		else:
			login_manager.fail(_('Incorrect Verification code'), login_manager.user)