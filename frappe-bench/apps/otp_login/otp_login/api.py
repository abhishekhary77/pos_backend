from __future__ import unicode_literals
import frappe
from base64 import b64encode, b64decode
# from profiles.api import create_device

def create_token(user):
    user_details = frappe.get_doc("User", user)
    api_secret = frappe.generate_hash(length=15)
    # if api key is not set generate api key
    if not user_details.api_key:
        api_key = frappe.generate_hash(length=15)
        user_details.api_key = api_key
    user_details.api_secret = api_secret
    user_details.flags.ignore_permissions = True
    user_details.save()

    token = "{}:{}".format(user_details.api_key, api_secret)
    enco_token = frappe.safe_decode(b64encode(frappe.safe_encode(token)))

    return {"token": enco_token,
             "user_details": user_details}

@frappe.whitelist(allow_guest=True)
def login():
    print(frappe.form_dict)
    frappe.local.login_manager.authenticate()
    user = frappe.local.login_manager.user
    token = create_token(user)
    
    # create_device(user)
    return token
    

@frappe.whitelist()
def logout():
    print(frappe.session.user)
    print("**************************************************")
    create_token(frappe.session.user)
    return {"True": True}