from __future__ import unicode_literals
import frappe

@frappe.whitelist()
def get_items():
    print("######################################################")
    # items = frappe.get_doc("Items", items)
    # category = frappe.get_doc("Category", category)
    items = frappe.get_all("Items",
                          fields=["name","item_name", "category", "description", "tax_category","cost","image"])
    
    cat = frappe.get_all("Category",
                          fields=["name","category_name"])
    
    return {
        "items" : items,
        "categories" : cat
    }