# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in reservations/__init__.py
from reservations import __version__ as version

setup(
	name='reservations',
	version=version,
	description='reservations',
	author='Abhishek Hary',
	author_email='abhishek@deienami.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
